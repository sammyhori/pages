# pages

This is my [static](https://sammyhori.com/glossary/#static-web) website hosted by Codeberg.org.

##### Status: Updated frequently

Check it out at: [https://sammyhori.com](https://sammyhori.com "Sammy Hori | Homepage")

The plan for this website is that it is just my personal site, which I post content and play with code (mostly HTML and CSS) on.

### Currently the content I post falls into four categories:

<dl>
	<dt>Posts</dt>
	<dl>General things such as small guides, ideas and tips.</dl>
	<dt>Projects</dt>
	<dl>Projects I'm working on/worked on.</dl>
</dl>

Enjoy!
